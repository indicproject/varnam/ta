/**
 * Thanks user207064 https://stackoverflow.com/a/31586010/1372424
 * Licensed under CC-BY-SA 3.0
 * Modifications made by Subin Siby <subinsb.com>, 2020
 */

package com.varnamproject.pack.ta;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static String TAG = "VarnamProvider";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String[] assetFilesList = null;
        // Get Asset Mangaer
        AssetManager assetManager = getAssets();
        try {
            assetFilesList = assetManager.list("");
        } catch (IOException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        // Set up an Intent to send back to apps that request files
        Intent mResultIntent = new Intent(this.getPackageName() + ".ACTION_SEND_MULTIPLE");
        // new Uri list
        ArrayList<Uri> uriArrayList = new ArrayList<>();
        // Set the Activity's result to null to begin with
        setResult(Activity.RESULT_CANCELED, null);

        Uri fileUri;
        if (assetFilesList != null) {
            for (String currFile : assetFilesList) {
                if (
                        !getFileExtension(currFile).equals("vst") &&
                        !getFileExtension(currFile).equals("gzip") &&
                        !getFileExtension(currFile).equals("json")
                ) {
                    // skip non vst, gz, json files. https://stackoverflow.com/q/6761008/1372424
                    continue;
                }
                Log.i(TAG, "Adding File " + currFile);
                // parse and create uri
                fileUri = Uri.parse("content://" + getString(R.string.authority_name) + "/" + currFile);
                // add current file uri to the list
                uriArrayList.add(fileUri);
            }
        }
        else {
            Log.e(TAG, "files array is pointing to null");
        }

        if (uriArrayList.size() != 0) {
            // Put the UriList Intent
            mResultIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uriArrayList);
            mResultIntent.setType("application/octet-stream");
            // Set the result
            this.setResult(Activity.RESULT_OK, mResultIntent);
        } else {
            // Set the result to failed
            mResultIntent.setDataAndType(null, "");
            this.setResult(RESULT_CANCELED, mResultIntent);
        }
        // Finish Activity and return Result to Caller
        finish();
    }

    private String getFileExtension(String file) {
        String extension = "";
        int i = file.lastIndexOf('.');
        if (i > 0) {
            extension = file.substring(i + 1);
        }
        return extension;
    }
}